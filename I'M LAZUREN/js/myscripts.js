$(function() {
				var $menu = $('nav#menu'),
					$html = $('html, body');

				$menu.mmenu({
					dragOpen: true
				});

				var $anchor = false;
				$menu.find( 'li > a' ).on(
					'click',
					function( e )
					{
						$anchor = $(this);
					}
				);

				var api = $menu.data( 'mmenu' );
				api.bind( 'closed',
					function()
					{
						if ( $anchor )
						{
							var href = $anchor.attr( 'href' );
							$anchor = false;

							//	if the clicked link is linked to an anchor, scroll the page to that anchor 
							if ( href.slice( 0, 1 ) == '#' )
							{
								$html.animate({
									scrollTop: $( href ).offset().top
								});	
							}
						}
					}
				);
			});
			var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

// Вертикальная диаграмма programming skills

			var barChartData = {
			labels : ["html5"],
			datasets : [
			{
				label: "html5",
	            fillColor: "rgba(192, 168, 234, 1)",
	            strokeColor: "rgba(192, 168, 234, 1)",
	            highlightFill: "rgba(192, 168, 234, 1)",
	            highlightStroke: "rgba(192, 168, 234, 1)",
	            data: [randomScalingFactor()]
			},
			{
				label: "css3",
	            fillColor: "rgba(255, 223, 124, 1)",
	            strokeColor: "rgba(255, 223, 124, 1)",
	            highlightFill: "rgba(255, 223, 124, 1)",
	            highlightStroke: "rgba(255, 223, 124, 1)",
	            data: [randomScalingFactor()]
			},
			{
				label: "php",
	            fillColor: "rgba(174, 199, 133, 1)",
	            strokeColor: "rgba(174, 199, 133, 1)",
	            highlightFill: "rgba(174, 199, 133, 1)",
	            highlightStroke: "rgba(174, 199, 133, 1)",
	            data: [randomScalingFactor()]
			},
			{
				label: "js",
	            fillColor: "rgba(178, 221, 247, 1)",
	            strokeColor: "rgba(178, 221, 247, 1)",
	            highlightFill: "rgba(178, 221, 247, 1)",
	            highlightStroke: "rgba(178, 221, 247, 1)",
	            data: [randomScalingFactor()]
			},
		]

		}
		
// Круговая диаграмма designing skills


	var doughnutData = [
				{
					value: 20,
					color:"rgba(192, 168, 234, 1)",
					highlight: "rgba(192, 168, 234, 1)",
					label: "HTML5"
				},
				{
					value: 30,
					color: "rgba(255, 223, 124, 1)",
					highlight: "rgba(255, 223, 124, 1)",
					label: "CSS3"
				},
				{
					value: 50,
					color: "rgba(174, 199, 133, 1)",
					highlight: "rgba(174, 199, 133, 1)",
					label: "JavaScript"
				},
				{
					value: 20,
					color: "rgba(178, 221, 247, 1)",
					highlight: "rgba(178, 221, 247, 1)",
					label: "PHP"
				}

			];

			// Вызов контекстов

			window.onload = function(){
			var ctx = document.getElementById("canvas").getContext("2d");
			window.myBarChart = new Chart(ctx).Bar(barChartData,{
				responsive : true,
				showScale: false,
				scaleOverride: false,
				showTooltips: false,
				barDatasetSpacing : 7,
				barValueSpacing : 0,
			});

			var a = document.getElementById("chart-area").getContext("2d");
				window.myDoughnut = new Chart(a).Doughnut(doughnutData, {
					responsive : true
				});
			};

			

			$(function(){

				// Вызов функции mixItUp
				$('#container').mixItUp();

				// Вызов функции tinycarousel
				$('#slider1').tinycarousel();

				
			});







				



