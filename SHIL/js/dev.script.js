// A $( document ).ready() block.
$( document ).ready(function() {

$('#category-1').metisMenu({});
$('#category-2').metisMenu({});
$('#category-3').metisMenu({});
$('#category-4').metisMenu({});
$('#category-5').metisMenu({});

$('#certificate').flickity({
	pageDots: false,
});
$('#partnrs').flickity({
	pageDots: false,
});

$( "#slider" ).slider({
      range: "min",
      value: 120,
      min: 30,
      max: 800,
      slide: function( event, ui ) {
      	$( "#amount" ).val(ui.value);
      }
    });
   $('#slider > span').append('<input type="text" id="amount" readonly value="120" /><p class="cvadr">M</p>');

    $("#phone").mask("+7 (999) 999-9999");
});
