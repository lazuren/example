<?php get_header() ?>
		<!-- section -->
		<main class="container">
		  <!-- Introductory text -->
		  <hgroup class="introductory-text">
			 <h3><?php echo get_cat_name(8) ?></h3>
			 <?php echo category_description(8); ?>
		  </hgroup>
		  <!-- end Introductory text -->
		  <!-- Passion Creativity Qality -->
		  <ul class="passion-creativity-qality clearfix">
			 
			 	
			<?php if ( have_posts() ) : query_posts('cat=8'); while (have_posts()) : the_post(); ?>
			<li>
				<div><?php the_post_thumbnail(array(220, 220)); ?></div>
				<h4><?php the_title(); ?></h4>
				<?php the_content() ?>
			<?php endwhile; endif; wp_reset_query();?>
		  </ul>
		  <!-- end Passion Creativity Qality -->
		</main>
		<!-- end section -->
		<!-- Video -->
		<div class="dg-video">
		  <section class="container video">
			<h3><?php echo get_cat_name(9) ?></h3>
			 <?php echo category_description(9); ?>
			 <div class="video-slideshow">
				<ul id="carusel">
					<?php if ( have_posts() ) : query_posts('cat=9'); while (have_posts()) : the_post(); ?>
						<li>
						 <?php the_post_thumbnail(); ?>
						<?php if( !has_post_thumbnail() ){the_excerpt();} ?>
						 <hgroup class="video-title">
							<h4><?php the_title(); ?></h4>
							<?php the_content() ?>
						 </hgroup>
					  </li>
					<?php endwhile; endif; wp_reset_query();?>
				</ul>
			 </div>
			  <i class="next" onclick="btn()">&#8249;</i>
			  <i class="prev" onclick="btn()">&#8250;</i>
			  <ul class="carusel-dot">
				 <li class="asd1" onclick="focusItem(1)"></li>
				 <li class="asd2" onclick="focusItem(2)"></li>
				 <li class="asd3" onclick="focusItem(3)"></li>
				 <li class="asd4" onclick="focusItem(4)"></li>
				 <li class="asd5 active" onclick="focusItem(5)"></li>
			  </ul>
			 <a href="<?php $options = get_option('sample_theme_options');echo $options['button_blue_conture_link']; ?>" class="view-more"><?php $options = get_option('sample_theme_options');echo $options['text_button_blue_conture']; ?></a>
		  </section>
		</div>
		<!-- end Video -->
		<!-- Download framework -->
		<div class="down-framework">
		  <section class="container">
			 <hgroup class="introductory-text df">
				<h3><?php $options = get_option('sample_theme_options');echo $options['bottom_title_text']; ?></h3>
				<p><?php $options = get_option('sample_theme_options');echo $options['bottom_description_text']; ?></p>
			 </hgroup>
			 <a href="" class="btn-download btn-df"><?php $options = get_option('sample_theme_options');echo $options['text_button_blue']; ?></a>
		  </section>
		</div>
<?php get_footer() ?>