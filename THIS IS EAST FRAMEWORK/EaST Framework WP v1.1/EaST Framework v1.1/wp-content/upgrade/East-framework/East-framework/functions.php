<?php 

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

show_admin_bar(false);

// Расширение для настройки темы
require_once ( get_stylesheet_directory() . '/theme-options.php' );


// Загружаемые стили и скрипты

function load_style_scripts() {
	// Скрипт карусели
	wp_enqueue_script( 'jquery.roundabout.min', get_bloginfo('template_url') .'/js/jquery.roundabout.min.js', array( 'jquery' ));
	// Скрипты разработчика
	wp_enqueue_script( 'dev.script',  get_bloginfo('template_url') .'/js/dev.script.js', array( 'jquery' ));

	// Основной файл стилей
	wp_enqueue_style('dev-style', get_bloginfo('template_url') .'/css/dev-style.css' );
	// Стили для различных типов устройств
	wp_enqueue_style('media-querys', get_bloginfo('template_url') .'/css/media-querys.css' );
	//Векторные иконки
	wp_enqueue_style('font-awesome', get_bloginfo('template_url') .'/css/font-awesome.min.css');
};

add_action( 'wp_enqueue_scripts', 'load_style_scripts' );


//======================================/ Поддержка миниатюр /==================================/

add_theme_support('post-thumbnails');



//========================================/ Регистрация меню /=================================/
register_nav_menus(array(
	'top_menu' => 'Верхнее меню',
	'min_menu' => 'Подвижное меню',
	));

//=====================================/ Регистрация виджет зон /=====================================/

register_sidebar(array(
	'name' => 'Сайтбар',
	'id' => 'sidebar',
	'description' => 'Сайтбар на отдельных страницах',
	'before_widget' => '<div class="sidebar">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3>',
	'after_title'   => '</h3>'

	));

