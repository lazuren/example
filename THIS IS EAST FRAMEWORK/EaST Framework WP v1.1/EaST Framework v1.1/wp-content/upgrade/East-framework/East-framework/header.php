<!DOCTYPE html>
<html lang="ru">
  <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		  <title><?php wp_title(); bloginfo();?></title>

	 <!-- Favocon -->
	 <link rel="icon" type="image/png" href="favicon.png" />
	 <link rel="apple-touch-icon-precomposed" href="apple-touch-favicon.png"/>

	 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	 <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	 <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	 <![endif]-->
	 <?php wp_head(); ?>
  </head>
  <body>
		<!-- header -->
		<header class="main-neader">
			<div class="bg-menu">
				<nav class="container">
				<a href="<?php echo home_url(); ?>" class="logo"></a>
				 <i class="gamburger" onclick="responsiveMenu('top')"></i>
				  <?php wp_nav_menu(array(
				'theme_location' => 'top_menu',
				'container' => false,
				'menu_class' => 'ddmenu',
				'menu_id' => 'ddmenu',

			 )); ?>
			</nav>
		</div>
		<div class="container">
		  <hgroup class="main-text">
			 <h1><?php $options = get_option('sample_theme_options');echo $options['main_title']; ?></h1>
			 <h2><?php $options = get_option('sample_theme_options');echo $options['main_description']; ?></h2>
		  </hgroup>
		  <a href="<?php $options = get_option('sample_theme_options');echo $options['ef_button_down_link']; ?>" class="btn-download"><?php $options = get_option('sample_theme_options');echo $options['text_button_blue']; ?><i><?php $options = get_option('sample_theme_options');echo $options['ef_button_ver_size']; ?></i></a>
		  
		</div>
		</header>
		<!-- end header -->
		<!-- Mini menu -->
			<div class="bg-menu bg-mini-menu" id="top-nav">
				<div class="container">
					<a href="<?php echo home_url(); ?>" class="logo mini-logo"></a>
					<i class="gamburger gamb-min-menu" onclick="responsiveMenu('mini')"></i>
					<?php wp_nav_menu(array(
						'theme_location' => 'top_menu',
						'container' => false,
						'menu_class' => 'ddmenu min-menu',
						'menu_id' => 'ddmenu',

					 )); ?>
				</div>
			</div>
			<!-- end mini menu -->