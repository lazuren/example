		<!-- end Download framework -->
		<!-- footer -->
		<footer class="footer">
		  <ul class="container main-who-we-are clearfix">
			 <li>
				<h5><?php $options = get_option('sample_theme_options');echo $options['about_footer_title']; ?></h5>
				<a href="" class="logo mini-logo bottom-logo"></a>
				<p class="bottom-logo-text">
				  <?php $options = get_option('sample_theme_options');echo $options['about_footer_text']; ?>
				</p>
			 </li>
				<?php if ( have_posts() ) : query_posts('cat=10'); while (have_posts()) : the_post(); ?>
					<li>
						<a class="news_link" href="<?php echo get_permalink(1);?>"><h5><?php the_title(); ?></h5></a>
						<?php the_content() ?>
					</li>
				<?php endwhile; endif; wp_reset_query();?>
		  </ul>
		  <div class="bg-copy">
			 <div class="container">
			 <p class="copy">&copy; <?= date(" Y ") ?><?php $options = get_option('sample_theme_options');echo $options['copy']; ?></p>
				<ul class="social">
				<?php if ( have_posts() ) : query_posts('cat=11'); while (have_posts()) : the_post(); ?>
					<?php the_excerpt(); ?>
				<?php endwhile; endif; wp_reset_query();?>
				</ul>
			 </div>
		  </div>
		</footer>
		<!-- end footer -->
	 <?php wp_footer(); ?> <!-- Для вызова элементов управления админ панели -->
  </body>
</html>