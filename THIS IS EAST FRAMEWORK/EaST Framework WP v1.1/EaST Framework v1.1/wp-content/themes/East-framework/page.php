
<?php get_header(); ?>  <!-- Подключаем header сайта -->

			<!-- Section -->
			<main class="container clearfix">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="content">
				<header><h2><?php the_title(); ?></h2></header>
					<article>
						<?php the_content(); ?>
					</article>
				</div>
			<?php endwhile; ?>
			<!-- post navigation -->
			<?php else: ?>
			<div class="content">
				<header><h2><?php the_title(); ?></h2></header>
					<article>
						<p>Страница не заплонена</p>
					</article>
				</div>
			<?php endif; ?> 
<?php get_sidebar(); ?>
			</main>
			<!-- end Section -->
			<!-- end Download framework -->
<?php get_footer(); ?> <!-- Подключаем footer сайта -->