$(document).ready(function() {

new WOW().init();


		$(window).scroll(function(){
        if ($(window).scrollTop() >= 80) {
            $('.nav_fixed').addClass('nav_fixed_active fadeInDown animated');
            $('.nav_visible').css('display', 'none');

            	$('.subscrable').parallax({
					imageSrc: 'img/bg_subscrable.png',
					position: '0 0'
				});
        }
        else {
            $('.nav_fixed').removeClass('nav_fixed_active');
            $('.nav_visible').css('display', 'block');
        }
    });
	
	$('.main_head').parallax({
			imageSrc: 'img/bg_head.jpg',
			position: '0 0'
		});	

	$(".top_menu").click(function() {
	  $(".sandwich").toggleClass("active");
	});

	

	$('.top_menu').click(function(){
		
		if($('.nav_mobile').attr('style')){
			$('.nav_mobile').fadeOut(500);
			$('.cl-effect li a').removeClass('fadeInUp animated');
			setTimeout("$('.nav_mobile').removeAttr('style')", 700)
		}
		else{
			$('.cl-effect li a').addClass('fadeInUp animated');
			$('.nav_mobile').fadeIn(500).css('position','fixed');

			
		}
		
	});

	$(function () {
		$('#reviews').newsTicker({
			interval: "15000",
			nextBtnDiv: "#nextDiv",
		});
	});




});

$(window).load(function() { 
	$(".loader_inner").fadeOut(); 
	$(".loader").delay(400).fadeOut("slow"); 
});